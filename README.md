# lcdsynth

Fluidsynth node wrapper with UI directly painted to a LCD screen using spi-lcd node driver found at gitlab.com/bastiyang/spi-lcd.

Requirements:

- NodeJS 14.15.1

Installation:
./install.sh
