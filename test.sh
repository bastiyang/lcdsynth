#!/bin/bash
if grep -Fxq "[Samples]" /etc/samba/smb.conf
then
    echo "Samba already installed"
else
    echo "[Samples]" >> /etc/samba/smb.conf
    echo "  comment = Samples" >> /etc/samba/smb.conf
    echo "  path = /lcdsynth/samples" >> /etc/samba/smb.conf
    echo "  browseable = yes" >> /etc/samba/smb.conf
    echo "  read only = no" >> /etc/samba/smb.conf
    echo "  guest ok = no" >> /etc/samba/smb.conf
fi