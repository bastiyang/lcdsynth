const LCD = require("spi-lcd");
const Alsa = require("./lib/interface/Alsa");
const SoundDevice = require("./lib/interface/SoundDevice");
const { get, set } = require("./lib/util/Config");

const Fluidsynth = require("./lib/interface/Fluidsynth");

LCD.UI.initCanvas(["res/fonts/font.fnt"], [], {
  width: 480,
  height: 320,
  rotation: LCD.Rotation.Up,
}).then((canvas) => {
  let data = null;
  async function initData() {
    if (!data) {
      const channels = await Fluidsynth.getChannels();
      const fonts = await Fluidsynth.getFonts();

      if (channels && fonts) {
        data = {
          channels,
          fonts,
          selectedChannel: "",
          selectedFont: -1,
          selectedBank: -1,
          selectedInstrument: -1,
        };
        channelSelector.setData(channels);
        fontSelector.setData(fonts);
      }
    }
  }

  const tabs = new LCD.Tab(0, 0, 480, 320);

  ////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////
  // Program select
  ////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////
  const channelSelector = new LCD.Selector(["- not loaded -"], 10, 10, 170, 40);
  channelSelector.setValueField("id");
  channelSelector.setTitleField("name");
  channelSelector.onChange = (key) => {
    data.selectedChannel = key;
  };

  const fontSelector = new LCD.Selector(["- not loaded -"], 190, 10, 280, 40);
  fontSelector.setValueField("id");
  fontSelector.setTitleField("name");
  fontSelector.onChange = (key, index) => {
    if (data) {
      const banks = [];
      const font = data.fonts[index];
      for (let i = 0; i < font.banks.length; i++) {
        const bank = font.banks[i];
        banks.push(new LCD.Text(0, 0, bank.id, "Arial"));
      }
      bankList.setData(banks);
      instrumentList.setData([]);
      data.selectedFont = index;
    }
  };

  const bankList = new LCD.List(10, 60, 170, 200, []);
  bankList.onChange = (component, index) => {
    if (data) {
      const instruments = [];
      const bank = data.fonts[data.selectedFont].banks[index];
      for (let i = 0; i < bank.instruments.length; i++) {
        const instrument = bank.instruments[i];
        instruments.push(new LCD.Text(0, 0, instrument.name, "Arial"));
      }
      instrumentList.setData(instruments);
      data.selectedBank = index;
    }
  };

  const instrumentList = new LCD.List(190, 60, 280, 200, []);
  instrumentList.onChange = (component, index) => {
    const fontId = data.fonts[data.selectedFont].id;
    const bankId = data.fonts[data.selectedFont].banks[data.selectedBank].id;
    const instrumentId =
      data.fonts[data.selectedFont].banks[data.selectedBank].instruments[index]
        .id;
    Fluidsynth.changeInstrument(
      fontId,
      bankId,
      data.selectedChannel,
      instrumentId
    );

    console.log(fontId, bankId, data.selectedChannel, instrumentId);
  };

  const programSelect = new LCD.Scroll(0, 0, 480, 290);
  programSelect.addContentChild(channelSelector);
  programSelect.addContentChild(fontSelector);
  programSelect.addContentChild(bankList);
  programSelect.addContentChild(instrumentList);
  tabs.addTab("Program", programSelect);

  ////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////
  // Input/Output select
  ////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////
  let inputs = Alsa.getDevices();
  let outputs = SoundDevice.getDevices();

  const inputSelectorLabel = new LCD.Text(40, 10, "Input", "Arial");
  const inputSelector = new LCD.Selector(inputs, 140, 10, 300, 40);
  inputSelector.setValueField("id");
  inputSelector.setTitleField("name");
  inputSelector.setSelectedValue(get("input"));
  inputSelector.onChange = async (key) => {
    set("input", key);
    await Fluidsynth.setInput(key);
    await initData();
  };

  const outputSelectorLabel = new LCD.Text(40, 60, "Output", "Arial");
  const outputSelector = new LCD.Selector(outputs, 140, 60, 300, 40);
  outputSelector.setValueField("id");
  outputSelector.setTitleField("name");
  outputSelector.setSelectedValue(get("output"));
  outputSelector.onChange = async (key) => {
    set("output", key);
    await Fluidsynth.setOutput(key);
    await initData();
  };

  const config = new LCD.Scroll(0, 0, 480, 290);
  config.addContentChild(inputSelectorLabel);
  config.addContentChild(inputSelector);
  config.addContentChild(outputSelectorLabel);
  config.addContentChild(outputSelector);
  tabs.addTab("Config", config);

  canvas.addChild(tabs);

  /*

  const bankSelector = new LCD.Selector(["- not loaded -"], 20, 180, 400, 40);
  bankSelector.onChange = (key) => {
    if (banks) {
      instrumentSelector.setData(
        banks[Object.keys(banks)[bankSelector.getSelected()]]
      );
    }
  };

  const instrumentSelector = new LCD.Selector(
    ["- not loaded -"],
    20,
    220,
    400,
    40
  );
  instrumentSelector.onChange = (key) => {
    console.log(
      fontSelector.getSelected(),
      bankSelector.getSelected(),
      channelSelector.getSelected(),
      instrumentSelector.getSelected()
    );
    Fluidsynth.changeInstrument(
      fontSelector.getSelected(),
      bankSelector.getSelected(),
      channelSelector.getSelected(),
      instrumentSelector.getSelected()
    );
  };

  Event.on(Event.Events.SynthStarted, async () => {
    const channels = await Fluidsynth.getChannels();
    const fonts = await Fluidsynth.getFonts();

    channelSelector.setData(channels);
    fontSelector.setData(fonts);
  });

  canvas.addChild(inputSelector);
  canvas.addChild(outputSelector);
  canvas.addChild(channelSelector);
  canvas.addChild(fontSelector);
  canvas.addChild(bankSelector);
  canvas.addChild(instrumentSelector);
  */
});
