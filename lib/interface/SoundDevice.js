const { execSync } = require("child_process");

const LIST_CARD_COMMAND = "cat /proc/asound/cards";

class SoundDevice {
  parseDevices(data) {
    const response = [];
    const split = data.split("\n");
    split.forEach((l) => {
      const index = l.indexOf("]:");
      if (index >= 0) {
        const raw = l.substr(0, index).trim().split("[");
        response.push({ id: raw[0].trim(), name: raw[1].trim() });
      }
    });
    return response;
  }

  getDevices() {
    const data = execSync(LIST_CARD_COMMAND);
    return this.parseDevices(`${data}`);
  }

  getDevice(id) {
    const devices = this.getDevices();
    for (let i = 0; i < devices.length; i++) {
      const device = devices[i];
      if (device.id === id) {
        return device;
      }
    }
    return null;
  }
}

module.exports = new SoundDevice();
