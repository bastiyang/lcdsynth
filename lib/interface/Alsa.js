const { execSync } = require("child_process");
const Logger = require("../util/Logger");

const LIST_ALSA_DEVICES_COMMAND = "aconnect -o";
const CONNECT_ALSA_DEVICES_COMMAND = "aconnect '%DEVICE1%':0 '%DEVICE2%':0";

class Alsa {
  parseDevices(data) {
    const response = [];
    const split = data.split("\n");
    split.forEach((l) => {
      if (l.indexOf("client") >= 0) {
        const id = l.substring(l.indexOf(" ") + 1, l.indexOf(":"));
        const name = l.substring(l.indexOf("'") + 1, l.lastIndexOf("'"));
        response.push({ id, name });
      }
    });
    return response;
  }

  getDevices() {
    const data = execSync(LIST_ALSA_DEVICES_COMMAND);
    return this.parseDevices(`${data}`);
  }

  getDevice(id) {
    const devices = this.getDevices();
    for (let i = 0; i < devices.length; i++) {
      const device = devices[i];
      if (device.id === id) {
        return device;
      }
    }
    return null;
  }

  connectDevices(device1, device2) {
    try {
      const command = CONNECT_ALSA_DEVICES_COMMAND.replace(
        "%DEVICE1%",
        device1
      ).replace("%DEVICE2%", device2);
      execSync(command);
    } catch (error) {
      Logger.log(error);
    }
  }
}

module.exports = new Alsa();
