const { spawn } = require("child_process");
const { get } = require("../util/Config");
const Alsa = require("./Alsa");
const Logger = require("../util/Logger");
const SoundDevice = require("./SoundDevice");

class Fluidsynth {
  currentInput = null;
  currentOutput = null;
  currentSynth = null;
  started = false;

  constructor() {}

  async setInput(input) {
    this.currentInput = input;
    await this.init();
  }

  async setOutput(output) {
    this.currentOutput = output;
    await this.init();
  }

  async init() {
    if (this.currentInput && this.currentOutput) {
      if (this.currentSynth) {
        this.currentSynth.kill();
        this.started = false;
      }

      const data = await new Promise((accept, reject) => {
        try {
          const cb = (rawData) => {
            const input = Alsa.getDevice(this.currentInput);
            const output = SoundDevice.getDevice(this.currentOutput);
            Alsa.connectDevices(input.name, get("synthProcessId"));
            Logger.log(
              `Synth Started on in: ${input.name} out: ${output.name}`
            );
            this.currentSynth.stdout.off("data", cb);
            accept(rawData);
          };

          this.currentSynth = spawn(
            "fluidsynth",
            [
              "-a alsa",
              "-c 15",
              `-p ${get("synthProcessId")}`,
              `-o audio.alsa.device=hw:${this.currentOutput}`,
              `${get("samples")}`,
            ],
            { shell: true }
          );
          this.currentSynth.stdout.on("data", cb);
        } catch (error) {
          Logger.log(`Synth error: ${error}`);
          reject(error);
        }
      });

      if (`${data}`.indexOf(">") >= 0) {
        this.started = true;
      } else {
        this.started = false;
      }
    }
    return null;
  }

  parseFonts(str) {
    if (str) {
      const response = [];
      const list = str.trim().split("\n");
      for (let i = 2; i < list.length; i++) {
        let data = list[i].trim().split(" ");
        response.push({
          id: data[0],
          name: data[data.length - 1].substr(
            data[data.length - 1].lastIndexOf("/") + 1
          ),
          banks: null,
        });
      }
      return response;
    }
    return null;
  }

  parseBanks(str) {
    if (str) {
      const response = [];
      const list = str.trim().split("\n");
      for (let i = 1; i < list.length; i++) {
        const data1 = list[i].substr(0, 7).split("-");
        const data2 = list[i].substr(8);

        const bankId = data1[0];
        const instrumentId = data1[1];

        let bank = null;
        for (let j = 0; j < response.length; j++) {
          if (response[j].id === bankId) {
            bank = response[j];
            break;
          }
        }
        if (!bank) {
          bank = { id: bankId, instruments: [] };
          response.push(bank);
        }
        bank.instruments.push({ id: instrumentId, name: data2 });
      }
      return response;
    }
    return null;
  }

  parseChannels(str) {
    if (str) {
      const response = [];
      const list = str.trim().split("\n");
      for (let i = 1; i < list.length; i++) {
        const data1 = list[i].trim().split(", ");
        const data2 = data1[0].split(" ");
        response.push({
          id: data2[1],
          name: data1[0],
          currentProgram: data1[1],
        });
      }
      return response;
    }
    null;
  }

  async getChannels() {
    if (this.started) {
      const channels = await new Promise((accept) => {
        const command = `channels\n`;
        const cb = (rawData) => {
          accept(this.parseChannels(this.normalizeRawData(rawData)));
          this.currentSynth.stdout.off("data", cb);
        };
        this.currentSynth.stdout.on("data", cb);
        this.currentSynth.stdin.write(command);
      });
      return channels;
    }
    return null;
  }

  async getFonts() {
    if (this.started) {
      const fonts = await new Promise((accept) => {
        const command = `fonts\n`;
        const cb = (rawData) => {
          accept(this.parseFonts(this.normalizeRawData(rawData)));
          this.currentSynth.stdout.off("data", cb);
        };
        this.currentSynth.stdout.on("data", cb);
        this.currentSynth.stdin.write(command);
      });

      for (let i = 0; i < fonts.length; i++) {
        const font = fonts[i];
        font.banks = await this.getBanks(font.id);
      }
      return fonts;
    }
    return null;
  }

  async getBanks(font) {
    if (this.started) {
      const banks = await new Promise((accept) => {
        const command = `inst ${font}\n`;
        const cb = (rawData) => {
          accept(this.parseBanks(this.normalizeRawData(rawData)));
          this.currentSynth.stdout.off("data", cb);
        };
        this.currentSynth.stdout.on("data", cb);
        this.currentSynth.stdin.write(command);
      });
      return banks;
    }
    return null;
  }

  async changeInstrument(font, bank, channel, instrument) {
    if (this.started) {
      const response = await new Promise((accept) => {
        const command = `select ${channel} ${font} ${bank} ${instrument}\n`;
        const cb = (rawData) => {
          accept(this.normalizeRawData(rawData));
          this.currentSynth.stdout.off("data", cb);
        };
        this.currentSynth.stdout.on("data", cb);
        this.currentSynth.stdin.write(command);
      });
      return response;
    }
    return null;
  }

  normalizeRawData(rawData) {
    const split = `${rawData}`.split(">");
    const last = split[split.length - 2];

    return last;
  }
}
module.exports = new Fluidsynth();
