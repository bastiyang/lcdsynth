const { spawn } = require("child_process");
const { getSocket } = require("../web/Socket");
const Alsa = require("./Alsa");
const Config = require("./util/Config");
const Event = require("./util/Event");
const Logger = require("./util/Logger");

let currentSynth = null;
let currentInput = null;
let currentOutput = null;

function init() {
  Event.on(Event.Events.SynthChangeInput, ({ input }) => {
    if (input !== currentInput) {
      currentInput = input;
      initSynth();
      Config.set("input", input);
    }
  });
  Event.on(Event.Events.SynthChangeOutput, ({ output }) => {
    if (output !== currentOutput) {
      currentOutput = output;
      initSynth();
      Config.set("output", output);
    }
  });
  Event.on(
    Event.Events.SynthChangeInstrument,
    ({ font, bank, channel, instrument }) => {
      changeInstrument(font, bank, channel, instrument);
    }
  );
  Event.on(Event.Events.SynthInit, ({ input, output }) => {
    if (input !== currentInput || output !== currentOutput) {
      Logger.log("Starting synth");
      currentInput = input;
      currentOutput = output;
      initSynth();
    } else {
      Event.emit(Event.Events.SynthStarted);
    }
  });
  Event.on(Event.Events.SynthStarted, async () => {
    const channels = await getChannels();
    const fonts = await getFonts();
    for (let k in fonts) {
      fonts[k].banks = await getBanks(k);
    }
    getSocket().emit("synthStarted", {
      channels,
      fonts,
    });
  });
}

function initSynth() {
  if (currentInput && currentOutput) {
    if (currentSynth) {
      currentSynth.kill();
    }

    currentSynth = spawn(
      "fluidsynth",
      [
        "-a alsa",
        "-c 15",
        `-p ${Config.get("synthProcessId")}`,
        `-o audio.alsa.device=hw:${currentOutput}`,
        `${Config.get("samples")}`,
      ],
      { shell: true }
    );

    currentSynth.stderr.on("data", (rawData) => {
      Logger.log(rawData);
    });

    let output = "";
    let firstTime = true;
    currentSynth.stdout.on("data", (rawData) => {
      let data = `${rawData}`;
      output += data;

      const split = output.split(">");
      output = split[split.length - 1];

      for (let i = 0; i < split.length - 1; i++) {
        if (!firstTime) {
          let d = split[i];
          let event = d.substr(0, d.indexOf("\n")).trim();
          Event.emit(event, { data: d });
        } else {
          firstTime = false;
          Alsa.connectDevices(
            Alsa.getDevices()[currentInput],
            Config.get("synthProcessId")
          );
          Event.emit(Event.Events.SynthStarted);
          Logger.log("Synth Started");
        }
      }
    });

    currentSynth.stdout.on("close", (code) => {
      Logger.log("Synth closed");
    });
  }
}

function parseFonts(str) {
  const response = {};
  const list = str.trim().split("\n");
  for (let i = 2; i < list.length; i++) {
    let data = list[i].trim().split(" ");
    response[data[0]] = {
      name: data[data.length - 1],
      banks: null,
    };
  }
  return response;
}

function parseBanks(str) {
  const response = {};
  const list = str.trim().split("\n");
  for (let i = 1; i < list.length; i++) {
    const data1 = list[i].substr(0, 7).split("-");
    const data2 = list[i].substr(8);
    if (!response[data1[0]]) {
      response[data1[0]] = {};
    }
    response[data1[0]][data1[1]] = data2;
  }
  return response;
}

function parseChannels(str) {
  const response = {};
  const list = str.trim().split("\n");
  for (let i = 1; i < list.length; i++) {
    const data1 = list[i].trim().split(", ");
    const data2 = data1[0].split(" ");
    response[data2[1]] = { name: data1[0], instrument: data1[1] };
  }
  return response;
}

function getChannels() {
  return new Promise((accept) => {
    const command = `channels\n`;
    const cb = ({ data }) => {
      accept(parseChannels(data));
      Event.off(command.trim(), cb);
    };
    Event.on(command.trim(), cb);
    currentSynth.stdin.write(command);
  });
}

function getFonts() {
  return new Promise((accept) => {
    const command = `fonts\n`;
    const cb = ({ data }) => {
      accept(parseFonts(data));
      Event.off(command.trim(), cb);
    };
    Event.on(command.trim(), cb);
    currentSynth.stdin.write(command);
  });
}

function getBanks(font) {
  return new Promise((accept) => {
    const command = `inst ${font}\n`;
    const cb = ({ data }) => {
      accept(parseBanks(data));
      Event.off(command.trim(), cb);
    };
    Event.on(command.trim(), cb);
    currentSynth.stdin.write(command);
  });
}

function changeInstrument(font, bank, channel, instrument) {
  return new Promise((accept) => {
    const command = `select ${channel} ${font} ${bank} ${instrument}\n`;
    const cb = ({ data }) => {
      accept(); // TODO Something with instrument change response
      Event.off(command.trim(), cb);
    };
    Event.on(command.trim(), cb);
    currentSynth.stdin.write(command);
  });
}

module.exports = {
  init,
};
