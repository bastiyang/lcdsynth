const fs = require("fs");
const { log } = require("./Logger");

const CONFIG_FILE = "config.json";
const defaults = {
  synthProcessId: "fluidsynth",
  input: null,
  output: null,
  samples: "./samples/*.sf2",
  port: 7000,
};

let config = null;

function valueOrDefault(value, def) {
  const response = {};
  Object.keys(def).forEach((k) => {
    response[k] = value[k] ? value[k] : def[k];
  });
  return response;
}

function load() {
  let values = defaults;
  try {
    values = JSON.parse(
      fs.readFileSync(CONFIG_FILE, { encoding: "utf8", flag: "r" })
    );
  } catch (error) {
    log(error);
  }
  config = valueOrDefault(values, defaults);
}

function save() {
  fs.writeFile(CONFIG_FILE, JSON.stringify(config, null, 2), function (err) {
    if (err) {
      log("There has been an error saving your configuration data.");
      log(err.message);
      return;
    }
  });
}

function get(key) {
  return config[key];
}

function set(key, value) {
  config[key] = value;
  save();
}

load();
module.exports = {
  get,
  set,
};
