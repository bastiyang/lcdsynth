#!/bin/bash
mkdir /lcdsynth || true
mkdir /lcdsynth/samples || true
apt-get install samba -y
if grep -Fxq "[Samples]" /etc/samba/smb.conf
then
    echo "Samba already installed"
else
    echo "[Samples]" >> /etc/samba/smb.conf
    echo "  comment = Samples" >> /etc/samba/smb.conf
    echo "  path = /lcdsynth/samples" >> /etc/samba/smb.conf
    echo "  browseable = yes" >> /etc/samba/smb.conf
    echo "  read only = no" >> /etc/samba/smb.conf
    echo "  guest ok = no" >> /etc/samba/smb.conf
fi
cp lib /lcdsynth/ -R
cp res /lcdsynth/ -R
cp config.json /lcdsynth/ -R
cp index.js /lcdsynth/ -R
cp package.json /lcdsynth/ -R
cp lcdsynth.service /etc/systemd/system/lcdsynth.service
cd /lcdsynth
npm update
systemctl stop lcdsynth.service || true
systemctl disable lcdsynth.service || true
systemctl enable lcdsynth.service
systemctl start lcdsynth.service